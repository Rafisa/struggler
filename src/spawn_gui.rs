use std::f64::consts::PI;

use sod_client::*;

use plain_ui::*;
use plain_widgets::*;
use plain_ui_piston::*;

use graphics::*;
use graphics::math::Matrix2d;
use opengl_graphics::*;
use piston::input::*;

#[derive(Debug)]
pub struct Grid {
    pub grid_size: [u32; 2],
    pub spawn_parts: Vec<SpawnPart>
}

#[derive(Debug)]
pub struct GridUi {
    pub sizes: [[u32; 2]; 4],
    pub obj_type_dropdown: bool,
    pub active_element: Option<u8>,
    pub mirror: bool,
    pub ang: u8,
    pub grid: Grid,
    pub pos: [f64; 2],
    pub zoom: f64
}

pub fn render_part<'a>(
    spawn_parts: &SpawnPart,
    textures: &[Texture],
    sizes: &[[u32; 2]; 4],
    r: &mut PistonRenderer<'a>,
    transform: &Matrix2d,
    alpha: f32
) {
    if textures.len() > spawn_parts.obj_type as usize {
        let size = sizes[spawn_parts.obj_type as usize];
        let size = [size[0] as f64, size[1] as f64];

        let (x, y) = if spawn_parts.ang % 2 == 0 {
            (size[0] / 2.0, size[1] / 2.0)
        } else {
            (size[1] / 2.0, size[0] / 2.0)
        };

        let obj_transform = if spawn_parts.mirror {
            transform
                .trans(spawn_parts.pos[0] as f64 + x,
                       spawn_parts.pos[1] as f64 + y)
                .rot_rad(spawn_parts.ang as f64 * PI / 2.0)
                .flip_h()
                .flip_v()
        } else {
            transform
                .trans(spawn_parts.pos[0] as f64 + x,
                       spawn_parts.pos[1] as f64 + y)
                .rot_rad(spawn_parts.ang as f64 * PI / 2.0)
                .flip_v()
        };

        let image = Image::new().rect([-size[0] / 2.0,
            -size[1] / 2.0,
            size[0],
            size[1]])
            .color([1.0, 1.0, 1.0, alpha]);

        image.draw(
            &textures[spawn_parts.obj_type as usize],
            &r.c.draw_state,
            obj_transform,
            r.g
        );
    }
}

impl GridUi {
    pub fn handle_keyboard_input(&mut self, connection: &mut Connection, key: Key, down: bool) {
        match (key, down) {
            (Key::Left, false) => self.mirror = true,
            (Key::Right, false) => self.mirror = false,
            (Key::Return, false) => {
                connection.send(Msg::Spawn(GridMsg::Start)).unwrap();
            }
            _ => ()
        }
    }

    pub fn read_msg(&mut self, msg: GridMsg) {
        match msg {
            GridMsg::AddPart(part) => self.grid.spawn_parts.push(part),
            GridMsg::ChangePart(i, part) => if self.grid.spawn_parts.len() > i as usize {
                self.grid.spawn_parts[i as usize] = part;
            },
            GridMsg::RemovePart(i) => if self.grid.spawn_parts.len() > i as usize {
                self.grid.spawn_parts.remove(i as usize);
            },
            GridMsg::Start => ()
        }
    }

    pub fn grid_view<'a>(
        &mut self,
        connection: &mut Connection,
        textures: &[Texture],
        r: &mut PistonRenderer<'a>,
        input: &InputState,
        area: &Rect
    ) {
        let transform = r.c.transform
            .flip_v()
            .trans(0.0, -area.size[1] as f64)
            .zoom(self.zoom)
            .trans(-self.pos[0], -self.pos[1]);

        for part in &self.grid.spawn_parts {
            render_part(part, textures, &self.sizes, r, &transform, 1.0);
        }

        let line = Line {
            color: [0.75, 0.75, 0.75, 1.0],
            radius: 0.5 / self.zoom,
            shape: line::Shape::Square,
        };

        let height = self.grid.grid_size[1] as f64;

        for i in 0..self.grid.grid_size[0] + 1 {
            r.g.line(&line, [i as f64, 0.0, i as f64, height], &r.c.draw_state, transform);
        }

        let width = self.grid.grid_size[0] as f64;

        for i in 0..self.grid.grid_size[1] + 1 {
            r.g.line(&line, [0.0, i as f64, width, i as f64], &r.c.draw_state, transform);
        }

        for pointer in &input.pointers {
            if pointer.pos[0] > area.pos[0] &&
                pointer.pos[1] > area.pos[1] &&
                pointer.pos[0] < area.pos[0] + area.size[0] &&
                pointer.pos[1] < area.pos[1] + area.size[1]
            {
                let pointer_pos = [
                    pointer.pos[0] as f64,
                    (area.size[1] - pointer.pos[1]) as f64
                ];

                let map_pos = [
                    pointer_pos[0] / self.zoom + self.pos[0],
                    pointer_pos[1] / self.zoom + self.pos[1]
                ];

                self.zoom += pointer.zoom[1] as f64 * 16.0;
                self.zoom = self.zoom.max(16.0).min(256.0);

                self.pos = [
                    map_pos[0] - (pointer_pos[0] / self.zoom),
                    map_pos[1] - (pointer_pos[1] / self.zoom)
                ];

                if pointer.down[1] {
                    self.pos[0] -= pointer.vel[0] as f64 / self.zoom;
                    self.pos[1] -= -pointer.vel[1] as f64 / self.zoom;
                }

                if let Some(active_element) = self.active_element {
                    let x = self.pos[0] + pointer_pos[0] / self.zoom;
                    let y = self.pos[1] + pointer_pos[1] / self.zoom;

                    if x >= 0.0 && y >= 0.0 {
                        let part = SpawnPart {
                            obj_type: active_element,
                            mirror: self.mirror,
                            pos: [x as u32, y as u32],
                            ang: self.ang
                        };

                        if pointer.release[0] {
                            connection.send(Msg::Spawn(
                                GridMsg::AddPart(part.clone())
                            )).unwrap();
                        }

                        if pointer.release[2] {
                            self.ang = (self.ang + 1) % 4;
                        }

                        render_part(&part, textures, &self.sizes, r, &transform, 0.5);
                    }
                }

            }
        }
    }

    pub fn update<'a>(&mut self,
                      connection: &mut Connection,
                      textures: &[Texture],
                      r: &mut PistonRenderer<'a>,
                      input: &InputState,
                      area: &Rect)
    {
        r.g.clear_color([0.0, 0.0, 0.0, 1.0]);

        let border = r.border_size(0);

        let grid_area = Rect {
            pos: [256.0 + border * 2.0, 0.0],
            size: [area.size[0] - (256.0 + border * 2.0), area.size[1]],
        };

        self.grid_view(connection, textures, r, input, &grid_area);

        r.rect_poly(&[[border; 2], [128.0 + border, area.size[1]]], area, (0, 1));
        for (i, texture) in textures[0..4].into_iter().enumerate() {
            if img_button(
                r,
                input,
                &Rect {
                    pos: [border, border + i as f32 * 128.0],
                    size: [128.0; 2]
                },
                area,
                self.active_element == Some(i as u8),
                texture
            ) {
                if self.active_element != Some(i as u8) {
                    self.mirror = false;
                    self.ang = 0;
                    self.active_element = Some(i as u8);
                }
            } else if self.active_element == Some(i as u8) {
                self.active_element = None
            }
        }
    }
}
