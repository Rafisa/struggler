extern crate time;
extern crate plain_ui;
extern crate plain_widgets;
extern crate plain_ui_piston;
extern crate piston;
extern crate graphics;
extern crate opengl_graphics;
extern crate glfw_window;
extern crate sod_client;
extern crate texture;
extern crate gl;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate rmp_serde as rmps;

mod game_state;
mod spawn_gui;

use std::f32;
use std::io;

use time::*;

use plain_ui::*;
use plain_widgets::*;
use plain_ui_piston::*;

use graphics::*;
use opengl_graphics::*;
use opengl_graphics::glyph_cache::GlyphCache;
use glfw_window::*;
use piston::window::*;
use piston::event_loop::*;
use piston::input::*;

use sod_client::*;

use spawn_gui::*;
use game_state::*;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Part {
    pub behaviour: u8,
    pub rectangles: Vec<([u32; 2], [u32; 2])>,
    pub data_connectors: Vec<[f32; 2]>,
    pub physical_connectors: Vec<([f32; 2], f32)>
}

impl Part {
    pub fn size(&self) -> [u32; 2] {
        let mut size = [0; 2];
        for rectangle in &self.rectangles {
            size[0] = std::cmp::max(size[0], rectangle.0[0] + rectangle.1[0]);
            size[1] = std::cmp::max(size[1], rectangle.0[1] + rectangle.1[1]);
        }
        size
    }
}

#[derive(Debug)]
pub struct Game {
    pub game_state: GameState,
    pub mouse_steering: bool,
    pub target_ang_vel: f32,
    pub forward: u32,
    pub backward: u32,
    pub left: u32,
    pub right: u32
}

impl Game {
    pub fn handle_keyboard_input(&mut self, key: Key, down: bool) {
        let power = if down { 128 } else { 0 };

        match key {
            Key::A => self.left = power,
            Key::D => self.right = power,
            Key::W => self.forward = power,
            Key::S => self.backward = power,
            _ => ()
        }
    }

    pub fn update<'a>(&mut self,
                      connection: &mut Connection,
                      textures: &[Texture],
                      delta_time: f32,
                      r: &mut PistonRenderer<'a>,
                      input: &InputState,
                      area: &Rect)
    {
        for pointer in &input.pointers {
            if pointer.press[0] {
                connection.send(Msg::Game(GameMsg::Connector(5, 16))).unwrap();
            }
            if pointer.release[0] {
                connection.send(Msg::Game(GameMsg::Connector(5, 0))).unwrap();
            }

            if pointer.press[2] {
                connection.send(Msg::Game(GameMsg::Connector(6, 16))).unwrap();
            }
            if pointer.release[2] {
                connection.send(Msg::Game(GameMsg::Connector(6, 0))).unwrap();
            }

            if pointer.release[1] {
                self.mouse_steering = !self.mouse_steering;
                self.target_ang_vel = 0.0;
            }

            if self.mouse_steering {
                self.target_ang_vel = -(pointer.pos[0] - area.size[0] / 2.0) / 128.0;
            }
        }

        self.send_engine_states(connection);

        self.game_state.update(delta_time);

        r.g.clear_color([0.0, 0.0, 0.0, 1.0]);
        let context = r.c.trans((area.size[0] / 2.0) as f64, (area.size[1] / 2.0) as f64);

        self.game_state.render(r.g, &context, textures);
    }

    pub fn send_engine_states(&self, connection: &mut Connection) {
        let diff_vel = self.game_state.ang_vel - self.target_ang_vel;

        let (rotate_left, rotate_right) = if diff_vel < 0.0 {
            ((diff_vel * -8.0) as u32, 0)
        } else if diff_vel > 0.0 {
            (0, (diff_vel * 8.0) as u32)
        } else { (0, 0) };

        // rotation
        connection.send(Msg::Game(
            GameMsg::Connector(0, rotate_left + self.right / 2)
        )).unwrap();
        connection.send(Msg::Game(
            GameMsg::Connector(2, rotate_right + self.left / 2)
        )).unwrap();
        connection.send(Msg::Game(
            GameMsg::Connector(11, rotate_right + self.right / 2)
        )).unwrap();
        connection.send(Msg::Game(
            GameMsg::Connector(13, rotate_left + self.left / 2)
        )).unwrap();

        // forward
        connection.send(Msg::Game(
            GameMsg::Connector(1, self.forward)
        )).unwrap();

        // backward
        connection.send(Msg::Game(
            GameMsg::Connector(12, self.backward)
        )).unwrap();
    }

    pub fn read_msg(&mut self, msg: GameMsg) {
        match msg {
            GameMsg::Sensor(ang, ang_vel, objs) => {
                self.game_state.ang = ang;
                self.game_state.ang_vel = ang_vel;
                self.game_state.objs = objs;
            },
            _ => ()
        }
    }
}

#[derive(Debug)]
pub enum Stage {
    Spawn(GridUi),
    Game(Game)
}

/// The application state, including the game state.
pub struct State {
    pub connection: Connection,
    pub textures: Vec<Texture>,
    pub parts: Vec<Part>,
    pub stage: Option<Stage>
}

impl State {
    pub fn new() -> io::Result<Self> {
        // get ip address form args or default to localhost
        let args: Vec<String> = std::env::args().collect();
        let addr = if args.len() == 2 { &args[1] } else { "127.0.0.1:1917" };

        Ok(Self {
            connection: Connection::new(addr)?,
            textures: load_textures("assets/textures.txt")?,
            parts: load_parts("assets/parts.txt")?,
            stage: None
        })
    }

    pub fn handle_keyboard_input(&mut self, key: Key, down: bool) {
        match self.stage {
            Some(Stage::Spawn(ref mut grid_ui)) =>
                grid_ui.handle_keyboard_input(&mut self.connection, key, down),
            Some(Stage::Game(ref mut game)) => game.handle_keyboard_input(key, down),
            None => ()
        }
    }

    pub fn update<'a>(&mut self,
                      delta_time: f32,
                      r: &mut PistonRenderer<'a>,
                      input: &InputState,
                      area: &Rect)
    {
        for msg in self.connection.rx.try_iter() {
            match msg {
                Msg::SpawnStage(size) => self.stage = Some(Stage::Spawn(GridUi {
                    sizes: {
                        let mut sizes = [[0; 2]; 4];
                        for (i, part) in self.parts.iter().enumerate() {
                            sizes[i] = part.size();
                        }
                        sizes
                    },
                    obj_type_dropdown: false,
                    active_element: None,
                    mirror: false,
                    ang: 0,
                    grid: Grid {
                        grid_size: size,
                        spawn_parts: Vec::new(),
                    },
                    pos: [0.0; 2],
                    zoom: 48.0,
                })),
                Msg::GameStage => self.stage = Some(Stage::Game(Game {
                    game_state: GameState::new(),
                    mouse_steering: false,
                    target_ang_vel: 0.0,
                    forward: 0,
                    backward: 0,
                    left: 0,
                    right: 0
                })),
                Msg::Spawn(msg) => {
                    if let Some(Stage::Spawn(ref mut grid_ui)) = self.stage {
                        grid_ui.read_msg(msg);
                    }
                }
                Msg::Game(msg) => {
                    if let Some(Stage::Game(ref mut game)) = self.stage {
                        game.read_msg(msg);
                    }
                }
            }
        }

        match self.stage {
            Some(Stage::Spawn(ref mut grid_ui)) => grid_ui.update(
                &mut self.connection,
                &self.textures,
                r,
                input,
                area
            ),
            Some(Stage::Game(ref mut game)) => game.update(
                &mut self.connection,
                &self.textures,
                delta_time,
                r,
                input,
                area
            ),
            None => ()
        }
    }
}

/// loads parts into vec
pub fn load_parts<P: AsRef<std::path::Path>>(path: P) -> std::io::Result<Vec<Part>> {
    use std::fs::File;
    use std::io::{ BufRead, BufReader };

    let file = File::open(path)?;
    let reader = BufReader::new(file);

    let mut parts = Vec::new();
    for line in reader.lines() {
        let file = File::open(format!("assets/{}", line?))?;

        parts.push(rmps::decode::from_read(file).unwrap());
    }
    Ok(parts)
}

/// loads textures into vec
fn load_textures<P: AsRef<std::path::Path>>(path: P) -> std::io::Result<Vec<Texture>> {
    use std::io::BufRead;

    let file = std::fs::File::open(path)?;
    let reader = std::io::BufReader::new(file);

    let mut textures = Vec::new();
    for line in reader.lines() {
        textures.push(Texture::from_path(format!("assets/{}", line?)).unwrap());
    }
    Ok(textures)
}

fn main() {
    let opengl = OpenGL::V3_2;

    let mut window: GlfwWindow = WindowSettings::new("struggler", [1280, 720])
        .opengl(opengl)
        .exit_on_esc(true)
        .build()
        .unwrap();

    let mut g = GlGraphics::new(opengl);

    let mut events = Events::new(EventSettings::new());

//    let mut glyph_cache = Some(GlyphCache::new(
//        "assets/DejaVuSansMono.ttf",
//        TextureSettings::new()
//    ).unwrap());
    let mut glyph_cache = None;

    let mut input = InputState {
        pointers: Vec::new()
    };

    if let Ok(mut state) = State::new() {
        println!("Connected to server.");

        let mut last_update = get_time();

        while let Some(e) = events.next(&mut window) {
            use piston::input::Input::*;

            match e {
                Press(button) => {
                    match button {
                        Button::Keyboard(key) => {
                            state.handle_keyboard_input(key, true);
                        }
                        Button::Mouse(mouse_button) => {
                            handle_mouse_button(&mut input, mouse_button, true);
                        }
                        _ => ()
                    }
                }
                Release(button) => {
                    match button {
                        Button::Keyboard(key) => {
                            state.handle_keyboard_input(key, false);
                        }
                        Button::Mouse(mouse_button) => {
                            handle_mouse_button(&mut input, mouse_button, false);
                        }
                        _ => ()
                    }
                }
                Move(motion) => handle_motion(&mut input, motion),
                Render(args) => {
                    let mut size = [0.0; 2];
                    g.draw(args.viewport(), |c, g| {
                        size = [args.width as f32, args.height as f32];

                        let mut renderer = PistonRenderer {
                            g: g,
                            c: &c,
                            char_cache: &mut glyph_cache
                        };

                        let time = get_time();
                        let delta_time = {
                            let time_diff = time - last_update;
                            time_diff.num_milliseconds() as f32 * 0.001
                        };
                        last_update = time;
                        let area = Rect { pos: [0.0; 2], size: size };
                        state.update(delta_time, &mut renderer, &input, &area);

                    });

                    reset_pointers(&mut input);
                },
                _ => ()
            }
        }
    } else {
        println!("Couldn't connect to server");
    }
}
