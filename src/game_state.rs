use std::f64;

use graphics::*;
use sod_client::GameObject;

#[derive(Clone, Debug)]
pub struct GameState {
    pub ang: f32,
    pub ang_vel: f32,
    pub objs: Vec<GameObject>
}

impl GameState {
    pub fn new() -> Self {
        GameState {
            ang: 0.0,
            ang_vel: 0.0,
            objs: Vec::new()
        }
    }

    pub fn update(&mut self, delta_time: f32) {
        self.ang += self.ang_vel * delta_time;
        for ref mut game_object in &mut self.objs {
            game_object.ang += game_object.ang_vel * delta_time;
            game_object.pos[0] += game_object.vel[0] * delta_time;
            game_object.pos[1] += game_object.vel[1] * delta_time;
        }
    }
}

pub trait Render {
    fn render<G: Graphics>(&self, graphics: &mut G, context: &Context, textures: &[G::Texture]);
}

impl Render for GameObject {
    fn render<G: Graphics>(&self, graphics: &mut G, context: &Context, textures: &[G::Texture]) {
        let transform = if self.mirror {
            context.transform.clone()
                .trans(self.pos[0] as f64,
                       self.pos[1] as f64)
                .rot_rad(self.ang as f64)
                .flip_h()
                .flip_v()
        } else {
            context.transform.clone()
                .trans(self.pos[0] as f64,
                       self.pos[1] as f64)
                .rot_rad(self.ang as f64)
                .flip_v()
        };

        let rect = [
            -self.size[0] as f64 * 0.5,
            -self.size[1] as f64 * 0.5,
            self.size[0] as f64,
            self.size[1] as f64
        ];

        let image = Image::new().rect(rect);


        if textures.len() > self.obj_type as usize {
            image.draw(
                &textures[self.obj_type as usize],
                &context.draw_state,
                transform,
                graphics
            );
        }
    }
}

impl Render for GameState {
    fn render<G: Graphics>(&self, graphics: &mut G, c: &Context, textures: &[G::Texture]) {
        let context = c
            .zoom(8.0)
            .rot_rad(self.ang as f64)
            .flip_v();

        for ref game_object in &self.objs {
            game_object.render(graphics, &context, textures);
        }
    }
}
